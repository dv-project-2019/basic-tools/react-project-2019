import React from 'react';
import logo from './logo.svg';
import './App.css';
import HomePage from './pages/HomePage/HomePage';
import { BrowserRouter, Route } from 'react-router-dom';
import ForumPage from './pages/ForumPage/ForumPage';
import TopHeadlinePage from './pages/TopHeadlinePage/TopHeadlinePage';
import TopHeadlineContentPage from './pages/TopHeadlinePage/TopHeadlineContentPage';
import TopicsNewsPage from './pages/ForumPage/TopicsNewsPage';
import ForumContentPage from './pages/ForumPage/ForumContentPage';

function App() {
  return (
    <BrowserRouter>
      {/* <Route path="/processLogin" render={() => {
        login()
        return <Redirect to="/users" />
      }} />
      <Route path="/processLogout" render={() => {
        logout()
        return <Redirect to="/login" />
      }} /> */}
      <Route path="/" component={HomePage} exact={true} />
      <Route path="/home" component={HomePage} />
      <Route path="/forum" component={ForumPage} exact={true} />
      <Route path="/forum/:topics" component={TopicsNewsPage} exact={true}/>
      <Route path="/forum/:topics/content/:id" component={ForumContentPage} />
      <Route path="/top" component={TopHeadlinePage} exact={true}/>
      <Route path="/top/content/:id" component={TopHeadlineContentPage} />
      {/* <PrivateRoute path="/users" component={Userpage} exact={true} />
      <PrivateRoute path="/users/:user_id/todos" component={TodoPage} />
      <PrivateRoute path="/users/:user_id/albums" component={AlbumPage} exact={true} />
      <PrivateRoute path="/users/:user_id/albums/:album_id" component={AlbumListPage} /> */}
    </BrowserRouter>
  );
}

export default App;
